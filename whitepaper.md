# Introduction

Currently, Web Applications ("Web Apps") dominate the development and usage landscape for the Internet. Decentralized Applications ("DApps") are emerging as a new paradigm for both developers and users. There are some clear differences in the requirements for  Web Apps versus DApps. The diagram below describes some of these differences and our approach to meeting these requirements.

![Cloud vs. Fog Infrastructure](./docs/wp_intro.png)

The degree of decentralization clearly distinquishes cloud from fog infrastructure. Cloud is more heavily centralized. Traditional Web Apps use a number of typical back-end services via
a browser or native app. These typically include storage, databases, micro-services in a compute environment(e.g. Docker/Kubernetes), as well
as standard best-effort networking. However, both Web Apps and DApps usually require a user interface that leverages their usual infrastructures, either cloud or fog. DApps
frequently need additional types of services. These include: interaction with a blockchain via smart contracts or directly; "bot" daemons which run
across decentralized servers and storage; as well as premium network services enabling
[oracles](https://cointelegraph.com/explained/defi-oracles-explained) to leverage consensus. The execution context may also be decentralized. With
new technologies such as (Deno)[https://deno.land/], code may be written isomorphically. This approach allows personalized daemons to execute both
on the client as well as the server. These daemons can remain suspended and then instantiated until needed.

In addition, the domain name system can be used in a traditional static way for Web Apps while DApps usually require new dynamic approaches.
In generaly, DApps require either existing services to be repurposed or new foundational decentralized facilities. Of course, the difference between
Web Apps and DApps becomes the degree to which their environments are decentralized.

<img style="display: block; margin-left: auto; margin-right: auto;" alt="3 Legs" src="./docs/3leg_ui_iso_defi.png"></a>)

Unlike traditional Web Apps, DApps require both inherently decentralized infrastructure as well as more a more traditional back-end web service.
Ephemeral Apps allow for isomorphic code execution which can be resumed and suspend on a user's client and decentralized servers. In addition,
traditional decentralized services including blockchains, composable smart contracts and Web3 interfaces.

# Project Philosophy

1.  Licensing: This project is currently under an MIT license. We wish to insure end-users may also have access to the code. This is currently not
    explicity required on existing FOSS licenses. In addition, we have an innovative approach mitigating against software patents outlined in #2.
2.  Patents: Our intention is to extend our selected FOSS license approach to create a defensive patent pool across projects. We will leverage our
    sponsorship approach, outlined in #3, to create the extended license. All of this work will cover the GitLab Crypteriat projects and should be
    considered work in progress currently.
3.  Open Source Project Sponsorship: Crypteriat projects in these repositories each will have a `.sponsorship.ts` file in their root directories.
    These files define the relationship between sponsors and benefactors in the open source ecosystem and per the licensing agreement discussed above.
    Ideally, benefactor projects will agree to the extended license which will also extend patent protection to all parties. We will be defining a
    governance model to determine how and when patent trolls attacks may be mitigated. We're laying out our philosophy, here, so that other
    like-minded parties can contribute, discuss, and recommend courses of action as the projects mature.
4.  JavaScript TypeScript, and Solidity Development Experiences: The Crypteriat projects contained these repositories require experience with
    with three main languages: TypeScript, JavaScript, and Solidity. Additional languages knowledge may be required, including Rust, as the Deno
    portion of the system evolves in the project. We aim to provide a first class experience for web developers using JavaScript, and smart contract
    Solidity developers to work with these projects.

# DApp Requirements

1. DApps inherently need decentralized infrastructure. Smart contracts leveraging Blockchain technology are typical required to run a DApp.
2. In addition, a traditional execution environment for Web Apps...
3. Suspend and resume isomorphic execution...
4. Individuated daemons for user bots and their required service...
5. Censorship resistant installation and deployment...
6. Mobile first user interface and hosting...

# Enabling Sovereign Identity & Decentralized Daemons

The current cloud model we use for Web Apps uses a centralized and shared execution approach. This approach works ideally for the existing
"surveillance capitalism" business model. Under new sovereign approaches, there are a number new potential business models including DeFi "bots"
working on behalf of investors, personal services (e.g. email, chat, micro-blogging), co-operatives based on DApps (e.g. "Blockchain Uber"), and
personal payment as well as subscription bots.

# Enabling Sovereign Services via Decentralized Service Daemons

# TypeScript/JavaScript First Development Experience

# Cross-Chain Solidity (and other) Smart Contract Support

# Technology Stack

## User Interface

AlephJS...

## Execution Environment

Deno for isomorphic execution...

### Decentralized Service Infrastructure

### Cross-Chain Interaction

### Network Services

#### Decentralized Naming

#### Permissioned Prioritization

#### Trustless Association

# Security Model

## Permissioned Access

## Trustless Operational Structure

## Secure Remote Password for Authentication for Composition

Difference between smart contract and DApp composition...

## Object Capability (OCAP)

## Making Ephemeral Links Cannonical

### W3 Standards

#### Decentralized Identifiers (DIds)

#### Linked Data Documents

# Composable Ephemeral Apps as DApps

## Ephemeral DApp Opportunities

# Project Evolution

# Conclusion
